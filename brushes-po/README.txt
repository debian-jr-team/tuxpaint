In this directory you'll find scripts for converting description files
(e.g. brushes) to and from gettext PO files, for use by translators.

Run the following two scripts (in this order) everytime a description
is added or changed, or a translation (PO file) is updated:

createpo.sh
createtxt.sh

Note: Never edit the translations in the .txt files,
or run the .py scripts directly.
